package br.com.teamcode.jobbe.model

import java.io.Serializable
import java.util.*

/**
 * Created by Pedro Lobato on 20/03/2018.
 */
class Professional(
        var id: String? = null,
        var name: String? = null,
        var description: String? = null,
        var cep: String? = null,
        var address: String? = null,
        var addressNumber: String? = null,
        var pfj: String? = null,
        var urlPhoto: String? = null,
        var email: String? = null,
        var phone: String? = null,
        var cellPhone: String? = null,
        var services: String? = null,
        var whatsApp: String? = null,
        var review: Float = 0F,
        var reviewAll: Float = 0F,
        var reviewCount: Long = 0,
        var createAt: Date? = null,
        var updateAt: Date? = null

) : Serializable