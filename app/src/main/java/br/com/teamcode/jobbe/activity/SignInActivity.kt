package br.com.teamcode.jobbe.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import br.com.teamcode.jobbe.R
import com.google.firebase.auth.*
import kotlinx.android.synthetic.main.activity_sign_in.*
import kotlinx.android.synthetic.main.loading.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*

class SignInActivity : AppCompatActivity() {

    private val auth = FirebaseAuth.getInstance()
    private val tag = "SIGN_IN"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        toolbar()
        button_login.setOnClickListener { signIn() }
        go_reset_password.setOnClickListener {
            startActivity(Intent(this, ResetPasswordActivity::class.java))
        }
    }

    private fun formIsValid(): Boolean {
        var isValid = true

        when {
            input_email.text.isEmpty() -> {
                input_layout_email.requestFocus()
                isValid = false
            }
            input_password.text.isEmpty() -> {
                input_layout_password.requestFocus()
                isValid = false
            }
            else -> {
                input_layout_email.isErrorEnabled = false
                input_layout_password.isErrorEnabled = false
            }
        }

        return isValid
    }

    override fun onRestart() {
        super.onRestart()
        renderLoading(false)
        finish()
    }

    private fun signIn() {
        if (formIsValid()) {
            renderLoading(true)
            auth.signInWithEmailAndPassword(input_email.text.toString(), input_password.text.toString())
                    .addOnSuccessListener {
                        startActivity(Intent(this, MainActivity::class.java))
                    }
                    .addOnFailureListener { exception ->
                        renderLoading(false)
                        when (exception) {
                            is FirebaseAuthInvalidCredentialsException -> {
                                when (exception.errorCode) {
                                    "ERROR_INVALID_EMAIL" -> {
                                        input_layout_email.error = getString(R.string.error_email_invalid)
                                        input_layout_email.requestFocus()
                                    }
                                    "ERROR_WRONG_PASSWORD" -> {
                                        input_layout_password.error = getString(R.string.error_password_wrong)
                                        input_layout_password.requestFocus()
                                    }
                                    else -> Log.w(tag, exception.errorCode)
                                }
                            }
                            is FirebaseAuthInvalidUserException -> {
                                when (exception.errorCode) {
                                    "ERROR_USER_NOT_FOUND" -> {
                                        input_layout_email.error = getString(R.string.error_email_not_found)
                                        input_layout_email.requestFocus()
                                    }
                                    else -> Log.w(tag, exception.errorCode)
                                }
                            }
                            else -> Log.w(tag, exception)
                        }
                    }
        }
    }

    private fun toolbar() {
        toolbar.title.visibility = View.VISIBLE
        toolbar.title.text = getText(R.string.label_login)

        toolbar.back.visibility = View.VISIBLE
        back.setOnClickListener { finish() }
    }

    private fun renderLoading(render: Boolean) {
        loading.visibility = if (render) View.VISIBLE else View.GONE
    }
}
