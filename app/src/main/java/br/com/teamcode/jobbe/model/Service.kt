package br.com.teamcode.jobbe.model

import java.io.Serializable


class Service(
        var id: String? = null,
        var description: String? = null,
        var tags: String? = null,
        var categoryId: String? = null
) : Serializable