package br.com.teamcode.jobbe.activity

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.fragment.HomeFragment
import br.com.teamcode.jobbe.fragment.MenuFragment
import br.com.teamcode.jobbe.fragment.NotificationsFragment
import br.com.teamcode.jobbe.model.Category
import br.com.teamcode.jobbe.model.Service
import br.com.teamcode.jobbe.util.Render
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import android.content.SharedPreferences
import java.text.SimpleDateFormat


class MainActivity : AppCompatActivity() {

    private val db = FirebaseFirestore.getInstance()
    private val user = FirebaseAuth.getInstance().currentUser

    private val categories = arrayListOf<Category>()
    private val suggestions = arrayListOf<Service>()

    private var id: String? = null
    private var name: String? = null
    private var photoUrl: Uri? = null
    private var email: String? = null
    private var isProfessional: Boolean = false
    private lateinit var snackBar: Snackbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        snackBar = Snackbar.make(wrap_snackbar, R.string.press_back_again, Snackbar.LENGTH_LONG)

        Render.loading(root_main, true)

        getServiceSuggestions()

        menuBottom()

        db.collection("category")
                .orderBy("description")
                .get()
                .addOnSuccessListener {
                    it.forEach {
                        categories.add(it.toObject(Category::class.java))
                    }
                }
                .addOnFailureListener {
                    Log.w("CATEGORIES", it)
                    Render.loading(root_main, false)
                }

        if (user != null) {
            reviewsListener()

            db.collection("user")
                    .document(user.uid)
                    .get()
                    .addOnSuccessListener {
                        id = user.uid
                        isProfessional = it.data["professional"] as Boolean
                        name = user.displayName
                        photoUrl = user.photoUrl
                        email = user.email

                        if (intent.extras["select_tab"] == "menu") {
                            menu_bottom.getTabAt(2)?.select()
                            setCurrentTabFragment(2)
                        } else {
                            setCurrentTabFragment(0)
                        }

                        Render.loading(root_main, false)
                    }
                    .addOnFailureListener {
                        Log.w("USER", it)
                        Render.loading(root_main, false)
                    }
        }

    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        if (count == 0) {
            if (snackBar.isShown) {
                updateLastLogin()
                finishAffinity()
                super.onBackPressed()
            } else {
                snackBar.show()
            }
        } else {
            supportFragmentManager.popBackStack()
        }
    }

    private fun getServiceSuggestions() {
        suggestions.clear()
        val categoryRef = db.collection("category")

        categoryRef
                .get()
                .addOnSuccessListener {
                    it.forEach {
                        if (it.exists()) {
                            Log.v("RESULT_CATEGORY", it.data.toString())

                            categoryRef
                                    .document(it.id)
                                    .collection("services")
                                    .get()
                                    .addOnSuccessListener {
                                        it.forEach {
                                            Log.v("RESULT_SERVICE", it.data.toString())
                                            if (it.exists()) {
                                                suggestions.add(it.toObject(Service::class.java))
                                            }
                                        }
                                    }
                        }
                    }
                }
                .addOnFailureListener {
                    Log.w("SERVICES_SUGGESTIONS", it)
                }
    }

    private fun parseDate(value: String): Date? {
        val cal = Calendar.getInstance()
        val sdf = SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH)
        cal.time = sdf.parse(value)

        return cal.time
    }

    private fun reviewsListener() {
        if (user?.uid != null) {
            val valueLastLogin = getPreferences(Context.MODE_PRIVATE)
                    .getString("last_login", Calendar.getInstance().time.toString())

            val lastLogin = parseDate(valueLastLogin)

            Log.v("LAST_LOGIN", lastLogin.toString())
            val professionalRef = db.collection("professional").document(user.uid)

            indicator_notification.visibility = View.GONE

            professionalRef.get()
                    .addOnCompleteListener {
                        professionalRef.collection("reviews")
                                .addSnapshotListener { snapshots, e ->
                                    if (e != null) {
                                        Log.w("GET_DATA", "Listen failed.", e)
                                        return@addSnapshotListener
                                    }
                                    if (!snapshots.isEmpty) {
                                        snapshots.forEach {
                                            val createAt = parseDate(it["createAt"].toString())

                                            if (createAt != null) {
                                                if (createAt > lastLogin) {
                                                    indicator_notification.visibility = View.VISIBLE
                                                }
                                            }
                                        }
                                    }
                                }
                    }
        }
    }

    private fun updateLastLogin() {
        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        with(sharedPref.edit()) {
            putString("last_login", Calendar.getInstance().time.toString())
            apply()
        }
    }

    private fun menuBottom() {
        menu_bottom.visibility = View.VISIBLE
        menu_bottom.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab) {
                setCurrentTabFragment(tab.position)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
            }
        })
    }

    private fun setCurrentTabFragment(selectedTabPosition: Int) {
        when (selectedTabPosition) {
            0 -> {
                val home = HomeFragment()
                val bundle = Bundle()

                bundle.putSerializable("categories", categories)
                bundle.putSerializable("suggestions", suggestions)
                home.arguments = bundle

                addFragment(home)
            }
            1 -> {
                addFragment(NotificationsFragment())
                indicator_notification.visibility = View.GONE
            }
            2 -> {
                val menu = MenuFragment()
                val bundle = Bundle()

                bundle.putString("id", id)
                bundle.putString("name", name)
                bundle.putString("email", email)
                bundle.putString("photoUrl", photoUrl.toString())
                bundle.putBoolean("isProfessional", isProfessional)
                menu.arguments = bundle

                addFragment(menu)
            }
        }
    }

    private fun addFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.content, fragment)
                .commit()
    }
}
