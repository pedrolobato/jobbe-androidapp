package br.com.teamcode.jobbe.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.model.User
import com.google.firebase.auth.*
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.loading.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*

class SignUpActivity : AppCompatActivity() {

    private val auth = FirebaseAuth.getInstance()
    private val tag = "SING_UP"
    private val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        toolbar()
    }

    private fun formIsValid(): Boolean {
        var isValid = true

        when {
            input_name.text.isEmpty() -> {
                input_layout_name.error = getString(R.string.error_required_name)
                input_layout_name.requestFocus()
                isValid = false
            }
            input_email.text.isEmpty() -> {
                input_layout_email.error = getString(R.string.error_required_email)
                input_layout_email.requestFocus()
                isValid = false
            }
            input_password.text.isEmpty() -> {
                input_layout_password.error = getString(R.string.error_required_password)
                input_layout_password.requestFocus()
                isValid = false
            }
            input_confirm_password.text.isEmpty() -> {
                input_layout_confirm_password.error = getString(R.string.error_required_confirm)
                input_layout_confirm_password.requestFocus()
                isValid = false
            }
            input_password.text.toString() != input_confirm_password.text.toString() -> {
                input_layout_confirm_password.error = getString(R.string.error_confirm_password)
                input_layout_confirm_password.requestFocus()
                isValid = false
            }
            else -> {
                input_layout_name.isErrorEnabled = false
                input_layout_email.isErrorEnabled = false
                input_layout_password.isErrorEnabled = false
                input_layout_confirm_password.isErrorEnabled = false
            }
        }

        return isValid
    }

    override fun onRestart() {
        super.onRestart()
        renderLoading(false)
        finish()
    }

    fun signUp(v: View) {

        if (formIsValid()) {
            renderLoading(true)
            auth.createUserWithEmailAndPassword(input_email.text.toString(), input_password.text.toString())
                    .addOnSuccessListener {
                        val user = it.user

                        db.collection("user")
                                .document(user.uid)
                                .set(User(user.uid))
                                .addOnSuccessListener {
                                    user.updateProfile(
                                            UserProfileChangeRequest.Builder()
                                                    .setDisplayName(input_name.text.toString()).build()
                                    )
                                    startActivity(Intent(this, MainActivity::class.java))
                                }
                    }
                    .addOnFailureListener {
                        renderLoading(false)
                        when (it) {
                            is FirebaseAuthWeakPasswordException -> {
                                input_layout_password.error = getString(R.string.error_weak_password)
                                input_layout_password.requestFocus()
                            }
                            is FirebaseAuthInvalidCredentialsException -> {
                                input_layout_email.error = getString(R.string.error_email_invalid)
                                input_layout_email.requestFocus()
                            }
                            is FirebaseAuthUserCollisionException -> {
                                input_layout_email.error = getString(R.string.error_user_exists)
                                input_layout_email.requestFocus()
                            }
                            else -> Log.w(tag, it)
                        }
                    }
        }
    }

    private fun toolbar() {
        toolbar.title.visibility = View.VISIBLE
        toolbar.title.text = getText(R.string.label_register)

        toolbar.back.visibility = View.VISIBLE
        back.setOnClickListener { finish() }
    }

    private fun renderLoading(render: Boolean) {
        loading.visibility = if (render) View.VISIBLE else View.GONE
    }
}
