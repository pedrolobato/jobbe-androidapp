package br.com.teamcode.jobbe.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.model.Professional
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_list_professional.view.*


class ItemListProfessionalAdapter(private val itemsProfessional: ArrayList<Professional>, private val listener: (Professional) -> Unit)
    : RecyclerView.Adapter<ItemListProfessionalAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_list_professional, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = itemsProfessional.size

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val professional = itemsProfessional[position]
        holder?.bind(professional, listener)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(professional: Professional, listener: (Professional) -> Unit) {
            Picasso.get().load(professional.urlPhoto).placeholder(R.drawable.ic_photo).into(itemView.photo)
            itemView.name.text = professional.name
            itemView.review_professional.rating = professional.review
            itemView.services.text = professional.services
            itemView.setOnClickListener { listener(professional) }
        }
    }

}