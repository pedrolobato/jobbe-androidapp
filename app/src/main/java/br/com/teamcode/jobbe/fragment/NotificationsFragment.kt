package br.com.teamcode.jobbe.fragment


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.activity.ReviewsActivity
import br.com.teamcode.jobbe.adapter.ItemListNotificationAdapter
import br.com.teamcode.jobbe.model.Notification
import br.com.teamcode.jobbe.util.Render
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.fragment_notifications.view.*
import kotlinx.android.synthetic.main.toolbar.view.*

class NotificationsFragment : Fragment() {

    private val db = FirebaseFirestore.getInstance()
    private val user = FirebaseAuth.getInstance().currentUser

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_notifications, container, false)

        toolbar(view)
        itemsNotification(view)

        return view
    }

    private fun toolbar(view: View) {
        view.toolbar.title.visibility = View.VISIBLE
        view.toolbar.line_bottom.visibility = View.VISIBLE

        view.toolbar.title.text = getString(R.string.notifications)
    }

    private fun itemsNotification(view: View) {
        Render.loading(view, true)
        if (user?.uid != null) {
            val professionalRef = db.collection("professional").document(user.uid)

            professionalRef.collection("notification")
                    .orderBy("time", Query.Direction.DESCENDING)
                    .limit(20)
                    .get()
                    .addOnSuccessListener {
                        val notifications = arrayListOf<Notification>()

                        it.forEach {
                            if (it.exists()) {
                                notifications.add(it.toObject(Notification::class.java))
                            }
                        }

                        val itemNotificationAdapter = ItemListNotificationAdapter(notifications, {
                            itemNotificationClicked()
                        })

                        view.items_notification.layoutManager = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
                        view.items_notification.adapter = itemNotificationAdapter

                        if (notifications.isEmpty()) {
                            Render.notFound(view, true, getString(R.string.not_found_notification), R.drawable.ic_notifications_none)
                        } else {
                            Render.notFound(view, false)
                        }

                        Render.loading(view, false)
                    }
        }
    }

    private fun itemNotificationClicked() {
        startActivity(Intent(this.context, ReviewsActivity::class.java))
    }
}
