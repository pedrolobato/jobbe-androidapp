package br.com.teamcode.jobbe.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.R.drawable.ic_photo
import br.com.teamcode.jobbe.model.Professional
import br.com.teamcode.jobbe.model.Service
import br.com.teamcode.jobbe.util.Render
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_profile_data.*
import kotlinx.android.synthetic.main.activity_profile_data.view.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*
import java.util.*


class ProfileDataActivity : AppCompatActivity() {

    private val db = FirebaseFirestore.getInstance()
    private val user = FirebaseAuth.getInstance().currentUser
    private val rootRef = FirebaseStorage.getInstance().reference

    private lateinit var professional: Professional

    private val categoryDescriptionAll = arrayListOf<String>()
    private var categoryIdAll = arrayListOf<String>()
    private var categoryIdSelected: String? = null
    private var categoryIndex = -1

    private val servicesAll = arrayListOf<Service>()
    private val servicesSelected = arrayListOf<Service>()
    private val servicesDescriptionAll = arrayListOf<String>()
    private val servicesDescriptionSelected = arrayListOf<String>()
    private val servicesChecked = arrayListOf<Int>()

    private val galleryIntent = 2

    private var uploadTask: UploadTask? = null

    private var editMode: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_data)

        editMode = intent.getBooleanExtra("editMode", false)

        if (editMode) {
            Render.loading(root_profile_data, true)

            getCategory()

            val professionalRef = db.collection("professional").document(user!!.uid)

            professionalRef
                    .get()
                    .addOnSuccessListener {
                        professional = it.toObject(Professional::class.java)

                        input_description.setText(professional.description)

                        professionalRef
                                .collection("services")
                                .get()
                                .addOnSuccessListener { services ->
                                    categoryIndex = categoryIdAll.indexOf(services.documents[0].getString("categoryId"))

                                    categoryIdSelected = categoryIdAll[categoryIndex]
                                    text_category.text = categoryDescriptionAll[categoryIndex]

                                    servicesDescriptionAll.clear()

                                    categoryIdSelected?.let {
                                        db.collection("category")
                                                .document(it)
                                                .collection("services")
                                                .get()
                                                .addOnSuccessListener {
                                                    it.forEach {
                                                        if (it.exists()) {
                                                            val description = it.data["description"].toString()

                                                            servicesAll.add(it.toObject(Service::class.java))
                                                            servicesDescriptionAll.add(description)
                                                        }
                                                    }

                                                    services.forEach {
                                                        val description = it.data["description"].toString()

                                                        servicesSelected.add(it.toObject(Service::class.java))
                                                        servicesDescriptionSelected.add(description)
                                                        servicesChecked.add(servicesDescriptionAll.indexOf(description))
                                                    }

                                                    if (servicesChecked.isNotEmpty()) {
                                                        text_subcategory.text = servicesDescriptionAll[servicesChecked[0]]

                                                        val items = servicesChecked.count() - 1

                                                        if (items > 0) {
                                                            more_category.visibility = View.VISIBLE
                                                            more_category.text = "+%d".format(items)
                                                        } else {
                                                            more_category.visibility = View.GONE
                                                        }
                                                    } else {
                                                        text_subcategory.text = getString(R.string.no_selected)
                                                        more_category.visibility = View.GONE
                                                    }

                                                    Render.loading(root_profile_data, false)

                                                    done.isEnabled = formIsValid()

                                                }
                                                .addOnFailureListener {
                                                    text_subcategory.text = getString(R.string.error_loading_services)
                                                    Log.w("SERVICES_DIALOG", it)
                                                }
                                    }
                                }
                                .addOnFailureListener {
                                    Render.loading(root_profile_data, false)
                                }

                    }
                    .addOnFailureListener {
                        Render.loading(root_profile_data, false)
                    }

        } else {
            professional = intent.extras.get("professional") as Professional
            getCategory()
        }

        toolbar()
        setPhoto(user?.photoUrl)

        done.isEnabled = false
        done.setOnClickListener { send() }

        pick_photo.setOnClickListener { pickPhoto() }
        select_category.setOnClickListener { selectCategory() }
        select_subcategory.setOnClickListener { selectSubcategory() }

        cancel_upload.setOnClickListener {
            label_photo.text = getString(R.string.label_photo_profile)

            progress_upload_photo.visibility = View.GONE
            cancel_upload.visibility = View.GONE

            Snackbar.make(root_profile_data, getString(R.string.canceled_upload),
                    Snackbar.LENGTH_SHORT).show()
            uploadTask?.cancel()
        }

        input_description.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                done.isEnabled = formIsValid()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun afterTextChanged(s: Editable?) {}

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == galleryIntent && resultCode == Activity.RESULT_OK) {
            if (data != null) uploadPhoto(data.data)
        }
    }

    private fun uploadPhoto(uri: Uri) {
        val photo = Calendar.getInstance().time

        progress_upload_photo.visibility = View.VISIBLE
        cancel_upload.visibility = View.VISIBLE

        label_photo.text = getString(R.string.loading)

        uploadTask = rootRef.child("${user?.uid}/$photo").putFile(uri)
        uploadTask?.let {
            it.addOnProgressListener {
                val progress = 100.0 * it.bytesTransferred / it.totalByteCount
                progress_upload_photo.progress = progress.toInt()
                done.isEnabled = formIsValid()
            }.addOnSuccessListener {
                val newPhoto = it.metadata?.downloadUrl

                setPhoto(newPhoto)

                user?.updateProfile(
                        UserProfileChangeRequest.Builder().setPhotoUri(newPhoto).build()
                )

                progress_upload_photo.visibility = View.GONE
                cancel_upload.visibility = View.GONE

                label_photo.text = getString(R.string.label_photo_profile)

                Snackbar.make(root_profile_data, getString(R.string.success_upload_photo),
                        Snackbar.LENGTH_SHORT).show()
            }.addOnFailureListener {
                Log.w("UPLOAD_PHOTO", it)

                progress_upload_photo.visibility = View.GONE
                cancel_upload.visibility = View.GONE

                label_photo.text = getString(R.string.label_photo_profile)

            }
        }
    }

    private fun toolbar() {
        toolbar.title.visibility = View.VISIBLE
        toolbar.line_bottom.visibility = View.VISIBLE
        toolbar.back.visibility = View.VISIBLE

        toolbar.title.text = getString(R.string.perfil_data)
        back.setOnClickListener {
            uploadTask?.cancel()
            onBackPressed()
        }
    }

    private fun setPhoto(uri: Uri?) {
        Picasso.get().load(uri).placeholder(ic_photo).into(root_profile_data.photo)
    }

    private fun pickPhoto() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
        } else {
            startActivityForResult(Intent(Intent.ACTION_PICK).setType("image/*"), galleryIntent)
        }

    }

    private fun getCategory() {
        db.collection("category")
                .orderBy("description")
                .get()
                .addOnSuccessListener {
                    it.forEach {
                        if (it.exists()) {
                            val id = it.data["id"].toString()
                            val description = it.data["description"].toString()

                            categoryIdAll.add(id)
                            categoryDescriptionAll.add(description)
                        }
                    }
                }
                .addOnFailureListener {
                    Log.w("GET_CATEGORY", it)
                }
    }

    private fun selectCategory() {
        val alert = AlertDialog.Builder(this, R.style.DialogTheme)
        alert.setTitle(R.string.label_category)

        val array = arrayOfNulls<String>(categoryDescriptionAll.size)

        alert.setSingleChoiceItems(categoryDescriptionAll.toArray(array), categoryIndex, { _, which ->
            categoryIndex = which
        })

        alert.setPositiveButton(getString(R.string.ok), { dialog, _ ->
            if (categoryIndex != -1) {
                categoryIdSelected = categoryIdAll[categoryIndex]
                text_category.text = categoryDescriptionAll[categoryIndex]

                text_subcategory.text = getString(R.string.loading)

                getServices()

                servicesSelected.clear()
                servicesChecked.clear()
                servicesDescriptionSelected.clear()
                more_category.visibility = View.GONE
            }


            done.isEnabled = formIsValid()
            dialog.dismiss()
        })

        alert.setNegativeButton(getString(R.string.cancel), { dialog, _ ->
            done.isEnabled = formIsValid()
            dialog.dismiss()
        })

        alert.show()

    }

    private fun getServices() {
        servicesAll.clear()
        servicesDescriptionAll.clear()

        categoryIdSelected?.let {
            db.collection("category")
                    .document(it)
                    .collection("services")
                    .get()
                    .addOnSuccessListener {
                        it.forEach {
                            if (it.exists()) {
                                val description = it.data["description"].toString()

                                servicesAll.add(it.toObject(Service::class.java))
                                servicesDescriptionAll.add(description)
                            }
                        }

                        text_subcategory.text = getString(R.string.no_selected)
                    }
                    .addOnFailureListener {
                        text_subcategory.text = getString(R.string.error_loading_services)
                        Log.w("SERVICES_DIALOG", it)
                    }
        }
    }

    private fun selectSubcategory() {
        val alert = AlertDialog.Builder(this, R.style.DialogTheme)
        alert.setTitle(R.string.label_subcategory)

        val servicesCheckedArray = BooleanArray(servicesDescriptionAll.size, { i ->
            servicesChecked.contains(i)
        })
        val array = arrayOfNulls<String>(servicesDescriptionAll.size)

        alert.setMultiChoiceItems(servicesDescriptionAll.toArray(array), servicesCheckedArray, { _, which, isChecked ->
            servicesCheckedArray[which] = isChecked
            if (isChecked) {
                servicesSelected.add(servicesAll[which])
                servicesChecked.add(which)
                servicesDescriptionSelected.add(servicesDescriptionAll[which])
            } else {
                servicesSelected.remove(servicesAll[which])
                servicesChecked.remove(which)
                servicesDescriptionSelected.remove(servicesDescriptionAll[which])
            }
        })

        alert.setPositiveButton(getString(R.string.ok), { dialog, _ ->
            if (servicesChecked.isNotEmpty()) {
                text_subcategory.text = servicesDescriptionAll[servicesChecked[0]]

                val items = servicesChecked.count() - 1

                if (items > 0) {
                    more_category.visibility = View.VISIBLE
                    more_category.text = "+%d".format(items)
                } else {
                    more_category.visibility = View.GONE
                }
            } else {
                text_subcategory.text = getString(R.string.no_selected)
                more_category.visibility = View.GONE
            }
            done.isEnabled = formIsValid()
            dialog.dismiss()
        })

        alert.setNegativeButton(getString(R.string.cancel), { dialog, _ ->
            done.isEnabled = formIsValid()
            dialog.dismiss()
        })

        alert.show()

    }

    private fun formIsValid(): Boolean {
        var isValid = true

        when {
            input_description.text.isEmpty() -> {
                isValid = false
            }
            text_category.text == getString(R.string.no_selected) -> {
                isValid = false
            }
            text_subcategory.text == getString(R.string.no_selected) -> {
                isValid = false
            }
        }

        return isValid

    }

    private fun send() {
        uploadTask?.cancel()
        if (formIsValid()) {

            professional.urlPhoto = user?.photoUrl.toString()
            professional.description = input_description.text.toString()
            professional.services = servicesDescriptionSelected.toString().replace("[\\[\\]]+".toRegex(), "")

            if (editMode) {
                startActivity(Intent(this, ContactDataActivity::class.java)
                        .putExtra("professional", professional)
                        .putExtra("services", servicesSelected)
                        .putExtra("editMode", true))
            } else {
                professional.review = 0F
                startActivity(Intent(this, ContactDataActivity::class.java)
                        .putExtra("professional", professional)
                        .putExtra("services", servicesSelected))
            }
        }
    }
}
