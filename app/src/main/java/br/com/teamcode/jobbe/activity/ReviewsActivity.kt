package br.com.teamcode.jobbe.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.adapter.ItemListReviewAdapter
import br.com.teamcode.jobbe.model.Review
import br.com.teamcode.jobbe.util.Render
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.activity_reviews.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*

class ReviewsActivity : AppCompatActivity() {

    private val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reviews)

        toolbar()
        reviews()
    }

    private fun toolbar() {
        toolbar.title.visibility = View.VISIBLE
        toolbar.line_bottom.visibility = View.VISIBLE
        toolbar.back.visibility = View.VISIBLE

        toolbar.title.text = getString(R.string.reviews)
        toolbar.back.setOnClickListener { finish() }
    }

    private fun reviews() {
        val itemsReview = arrayListOf<Review>()

        val uid = intent.getStringExtra("id") ?: FirebaseAuth.getInstance().currentUser?.uid

        if (uid != null) {
            Render.loading(root_reviews, true)

            val professionalRef = db.collection("professional").document(uid)

            professionalRef.get()
                    .addOnSuccessListener {
                        val review = it.data["review"].toString().toFloat()

                        review_number.text = String.format("%.1f", review)
                        review_view_general.rating = review
                    }

            professionalRef
                    .collection("reviews")
                    .orderBy("createAt", Query.Direction.DESCENDING)
                    .limit(4)
                    .get()
                    .addOnSuccessListener {
                        if (it.isEmpty) {
                            Render.notFound(root_reviews, true,
                                    getString(R.string.not_found_reviews), R.drawable.ic_star_border)
                        } else {
                            it.forEach {
                                itemsReview.add(it.toObject(Review::class.java))
                            }
                            items_review.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                            items_review.adapter = ItemListReviewAdapter(itemsReview)
                            items_review.isNestedScrollingEnabled = false
                        }

                        Render.loading(root_reviews, false)
                    }
                    .addOnFailureListener {
                        Log.w("GET_REVIEWS", it)
                        Render.loading(root_reviews, false)
                    }
        }
    }
}
