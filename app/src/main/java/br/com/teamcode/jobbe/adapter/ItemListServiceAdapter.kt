package br.com.teamcode.jobbe.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.model.Service
import kotlinx.android.synthetic.main.item_list_text.view.*


class ItemListServiceAdapter(private val itemsService: List<Service>, private val listener: (Service) -> Unit)
    : RecyclerView.Adapter<ItemListServiceAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_list_text, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = itemsService.size

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val itemCategory = itemsService[position]
        holder?.bind(itemCategory, listener)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(service: Service, listener: (Service) -> Unit) {
            itemView.description.text = service.description
            itemView.setOnClickListener { listener(service) }
        }
    }
}