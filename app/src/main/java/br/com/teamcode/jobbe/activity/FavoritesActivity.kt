package br.com.teamcode.jobbe.activity

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.adapter.ItemListProfessionalAdapter
import br.com.teamcode.jobbe.model.Professional
import br.com.teamcode.jobbe.util.Render
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_favorites.*
import kotlinx.android.synthetic.main.loading.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*

class FavoritesActivity : AppCompatActivity() {

    private val db = FirebaseFirestore.getInstance()
    private val user = FirebaseAuth.getInstance().currentUser
    private var favoritesRef: CollectionReference? = null

    private var itemListProfessionalAdapter: ItemListProfessionalAdapter? = null
    private val itemsDialog = arrayOf("Ver Perfil", "Remover")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorites)

        favoritesRef = user?.let {
            db.collection("user").document(it.uid).collection("favorites")
        }

        renderLoading(true)

        toolbar()
        itemsFavorites()
    }

    private fun toolbar() {
        toolbar.title.visibility = View.VISIBLE
        toolbar.line_bottom.visibility = View.VISIBLE
        toolbar.back.visibility = View.VISIBLE

        toolbar.title.text = getString(R.string.favorites)
        back.setOnClickListener { onBackPressed() }
    }

    private fun itemsFavorites() {
        val favorites = arrayListOf<Professional>()

        favoritesRef?.let {
            it.get().addOnSuccessListener {
                it.forEach {
                    favorites.add(it.toObject(Professional::class.java))
                }

                itemListProfessionalAdapter = ItemListProfessionalAdapter(favorites, {
                    itemFavoriteClicked(it)
                })
                items_favorite.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                items_favorite.adapter = itemListProfessionalAdapter

                renderLoading(false)

                if(favorites.isEmpty()){
                    Render.notFound(root_favorites, true, getString(R.string.not_found_favorites), R.drawable.ic_favorite_none)
                }else{
                    Render.notFound(root_favorites, false)
                }

            }.addOnFailureListener {
                Log.w("LIST_FAVORITES", it)
            }
        }
    }

    private fun itemFavoriteClicked(professional: Professional) {
        val alert = AlertDialog.Builder(this, R.style.DialogTheme)
        alert.setTitle(professional.name)

        alert.setItems(itemsDialog, { _, which ->
            when (which) {
                0 -> {
                    startActivity(Intent(this, ProfileProfessionalActivity::class.java)
                            .putExtra("id", professional.id))
                }
                1 -> {
                    removeFavorite(professional)
                }
            }
        })

        alert.show()
    }

    private fun removeFavorite(professional: Professional) {
        val alert = AlertDialog.Builder(this, R.style.DialogTheme)

        alert.setTitle(professional.name)
        alert.setMessage("Deseja remover ${professional.name} dos favoritos?")

        alert.setPositiveButton("Sim", { dialog, _ ->
            dialog.dismiss()
            renderLoading(true)

            favoritesRef?.let {
                it.document(professional.id!!).delete()
                        .addOnSuccessListener {
                            itemsFavorites()
                            Snackbar.make(root_favorites, getString(R.string.success_remove_favorite),
                                    Snackbar.LENGTH_SHORT).show()
                        }
                        .addOnFailureListener {
                            Log.w("REMOVE_FAVORITE", it)
                            Snackbar.make(root_favorites, getString(R.string.error_remove_favorite),
                                    Snackbar.LENGTH_SHORT).show()
                        }
            }
        })

        alert.setNegativeButton("Não", { dialog, _ ->
            dialog.dismiss()
        })

        alert.show()
    }

    private fun renderLoading(render: Boolean) {
        loading.visibility = if (render) View.VISIBLE else View.GONE
    }
}
