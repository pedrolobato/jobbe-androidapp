package br.com.teamcode.jobbe.util

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText


object Mask {

    fun unmask(s: String): String {
        return s.replace("[.()/\\-\\s]+".toRegex(), "")
    }

    fun insert(input: EditText, mask: String, mask2: String = "", test: Int = -1): TextWatcher {
        return object : TextWatcher {
            internal var isUpdating = false
            internal var old = ""

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (isUpdating) {
                    isUpdating = false
                    old = s.toString()
                    return
                }

                if (s.toString().isNotEmpty() && s.toString().length > old.length) {
                    val str = Mask.unmask(s.toString())
                    var setMask = mask
                    if(str.length > test && test != -1) setMask = mask2
                    var formatter = ""

                    var i = 0
                    for (m in setMask.toCharArray()) {
                        if (m != '#') {
                            formatter += m
                            continue
                        }
                        try {
                            formatter += str[i]
                        } catch (e: Exception) {
                            break
                        }

                        i++
                    }
                    isUpdating = true
                    input.setText(formatter)
                    input.setSelection(formatter.length)
                } else {
                    old = s.toString()
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun afterTextChanged(s: Editable) {}
        }
    }

}