package br.com.teamcode.jobbe.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.model.Category
import kotlinx.android.synthetic.main.item_list_text.view.*


class ItemListCategoryAdapter(private val itemsCategory: ArrayList<Category>, private val listener: (Category) -> Unit)
    : RecyclerView.Adapter<ItemListCategoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_list_text, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = itemsCategory.size

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val itemCategory = itemsCategory[position]
        holder?.bind(itemCategory, listener)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(category: Category, listener: (Category) -> Unit) {
            itemView.description.text = category.description
            itemView.setOnClickListener { listener(category) }
        }
    }

}