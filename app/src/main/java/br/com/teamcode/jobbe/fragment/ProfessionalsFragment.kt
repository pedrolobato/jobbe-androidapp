package br.com.teamcode.jobbe.fragment


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.activity.ProfileProfessionalActivity
import br.com.teamcode.jobbe.adapter.ItemListProfessionalAdapter
import br.com.teamcode.jobbe.model.Professional
import br.com.teamcode.jobbe.model.Service
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_professionals.view.*
import kotlinx.android.synthetic.main.loading.view.*
import kotlinx.android.synthetic.main.not_found.view.*
import kotlinx.android.synthetic.main.toolbar.view.*

class ProfessionalsFragment : Fragment() {

    private val db = FirebaseFirestore.getInstance()
    private var itemListProfessionalAdapter: ItemListProfessionalAdapter? = null

    private val professionals = arrayListOf<Professional>()

    private var service: Service? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_professionals, container, false)

        service = this.arguments?.get("service") as Service

        renderLoading(view, true)
        getProfessionals(view)
        toolbar(view)
        itemsProfessional(view)

        return view
    }

    private fun toolbar(view: View) {
        view.toolbar.title.visibility = View.VISIBLE
        view.toolbar.back.visibility = View.VISIBLE
        view.toolbar.line_bottom.visibility = View.VISIBLE

        view.toolbar.title.text = service?.description
        view.toolbar.back.setOnClickListener { activity?.onBackPressed() }
    }

    private fun getProfessionals(view: View) {
        db.collection("professional")
                .get()
                .addOnSuccessListener {
                    it.documents.forEach {
                        val services = it.data["services"].toString().toLowerCase()
                        val description = service!!.description!!.toLowerCase()

                        if (services.contains(description)) {
                            professionals.add(it.toObject(Professional::class.java))
                        }
                    }

                    renderLoading(view, false)

                    if (professionals.isEmpty()) {
                        renderNotFound(view, true,
                                getString(R.string.not_found_professional),
                                R.drawable.ic_sentiment_dissatisfied)
                    } else {
                        renderNotFound(view, false)
                    }

                    itemListProfessionalAdapter?.notifyDataSetChanged()
                }
                .addOnFailureListener {
                    Log.w("GET_PROFESSIONALS", it)
                }
    }

    private fun itemsProfessional(view: View) {
        itemListProfessionalAdapter = ItemListProfessionalAdapter(professionals, {
            itemProfessionalClicked(it)
        })

        view.items_professional.layoutManager = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        view.items_professional.adapter = itemListProfessionalAdapter
        view.items_professional.isNestedScrollingEnabled = false
    }

    private fun itemProfessionalClicked(professional: Professional) {
        startActivity(Intent(this.context, ProfileProfessionalActivity::class.java)
                .putExtra("id", professional.id))
    }

    private fun renderLoading(view: View, render: Boolean) {
        view.loading.visibility = if (render) View.VISIBLE else View.GONE
    }

    private fun renderNotFound(view: View, render: Boolean, message: String? = null, icon: Int? = null) {
        view.not_found.visibility = if (render) View.VISIBLE else View.GONE
        view.not_found.message.text = message
        if (icon != null) {
            view.not_found.icon.setImageResource(icon)
        }
    }

}
