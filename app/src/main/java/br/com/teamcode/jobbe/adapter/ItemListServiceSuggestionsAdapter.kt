package br.com.teamcode.jobbe.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.teamcode.jobbe.R
import br.com.teamcode.jobbe.model.Service
import kotlinx.android.synthetic.main.item_list_text.view.*


class ItemListServiceSuggestionsAdapter(private val itemsService: ArrayList<Service>, private val listener: (Service) -> Unit)
    : RecyclerView.Adapter<ItemListServiceSuggestionsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_list_suggestions, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = itemsService.size

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val itemCategory = itemsService[position]
        holder?.bind(itemCategory, listener)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(service: Service, listener: (Service) -> Unit) {
            itemView.description.text = service.description
            itemView.setOnClickListener { listener(service) }
        }
    }

    private val itemsServiceCopy = itemsService.clone() as ArrayList<Service>

    fun filter(text: String) {
        itemsService.clear()
        if (text.isEmpty()) {
            itemsService.addAll(itemsServiceCopy)
        } else {
            val text = text.toLowerCase()
            for (row in itemsServiceCopy) {
                if (row.tags != null && row.description != null) {
                    if (row.tags.toString().toLowerCase().contains(text) ||
                            row.description.toString().toLowerCase().contains(text)) {
                        itemsService.add(row)
                    }
                }
            }
        }
        notifyDataSetChanged()
    }

}